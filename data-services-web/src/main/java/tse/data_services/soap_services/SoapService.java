package tse.data_services.soap_services;

import javax.jws.WebMethod;
import javax.jws.WebService;

import tse.data_services.Usuario;

@WebService(serviceName = "PDIServices")
public class SoapService {
	
	@WebMethod
	public Usuario getDatosUsuario(String cedula) {
		Usuario user = new Usuario();
		user.setCedula(cedula);
		user.setApellido("Bochkariov");
		user.setNombre("Aleksei");
		user.setCelular("099915953");
		user.setDireccion("21 de septiembre 2993");
		return user;
	}

}
