package tse.data_services.rest_services;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.stream.IntStream;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;


@Path("/api")
@Produces({ "application/xml", "application/json" })
@Consumes({ "application/xml", "application/json" })
public class BalanzaService {
	
	private final int MINIMO_CANTIDAD_PESAJES = 1;
	private final int MAXIMO_CANTIDAD_PESAJES = 5;
	private final int MINIMO_PESO = 5;
	private final int MAXIMO_PESO = 100;

	@GET
	@Path("/obtenerPesajes")
	@Produces(MediaType.APPLICATION_JSON)
	public Response ObtenerVehiculos() {
		List<Double> pesajes = new ArrayList<>();
		Random random = new Random();
		int numeroDePesajes = random.nextInt(MAXIMO_CANTIDAD_PESAJES) + MINIMO_CANTIDAD_PESAJES;
		IntStream.range(1, numeroDePesajes).forEach( i -> {
			Double pesoRandom = MINIMO_PESO + (random.nextDouble() * (MAXIMO_PESO - MINIMO_PESO));
			Double redondeado = Math.round(pesoRandom * 10) / 10.0;
			pesajes.add(redondeado);
		});
		return Response.ok(pesajes).build();
	}
}
