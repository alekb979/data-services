package tse.data_services;

import java.io.Serializable;

import lombok.Data;

@Data
@SuppressWarnings("serial")
public class Usuario implements Serializable {
	
	private String nombre;
	private String apellido;
	private String cedula;
	private String direccion;
	private String celular;

}
